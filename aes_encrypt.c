#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#define AES256 1

unsigned int aes = 128;
unsigned int word = 32;
#ifdef AES256
unsigned int N = 8;
unsigned int R = 15;
#endif
#ifdef AES128
unsigned int N = 4;
unsigned int R = 11;
#endif
unsigned char galois_multiplication(unsigned char a, unsigned char b);
void          build_tables(unsigned char num);
unsigned char get_inverse(unsigned char a);
unsigned char get_sbox_value(unsigned char a);
unsigned int  rotate(unsigned int a);
unsigned int  round_constant(unsigned char num);
void          add_round_key(unsigned int a, unsigned int b);
void          transpose_matrix(unsigned int row[4]);

void dump_expanded_key(unsigned int expanded[11][8]);
unsigned char collect_input(unsigned int text[4]);

unsigned char expo_table[256] = {0};
unsigned char log_table[256] = {0};



int main()
{
    

    //unsigned int key[4] = {0x00010203, 0x04050607, 0x08090a0b, 0x0c0d0e0f};
    //unsigned int key[4] = {0x2b7e1516, 0x28aed2a6, 0xabf71588, 0x09cf4f3c};
    //unsigned int key[8] = {0x603deb10, 0x15ca71be, 0x2b73aef0, 0x857d7781,
                          // 0x1f352c07, 0x3b6108d7, 0x2d9810a3, 0x0914dff4};
    unsigned int key[8] = {0x00010203, 0x04050607, 0x08090a0b, 0x0c0d0e0f,
                           0x10111213, 0x14151617, 0x18191a1b, 0x1c1d1e1f};
    unsigned int plaintext[4] = {0x00112233, 0x44556677, 0x8899aabb, 0xccddeeff};
    unsigned int rcon = 1;
    unsigned int k = 0;
    unsigned int w = 0;
    unsigned int i = 0;
    unsigned int iteration = 0;
    //unsigned int expanded_key[11][4] = {0};
    unsigned int expanded_key[15][8] = {0};
    unsigned int temp = 0;
    /* This will also become 2D (11 rounds) */
    unsigned int cipher_key[4] = {0};
    unsigned int bitshift = 0;
    unsigned int last_key[4] = {0};
    unsigned int status = 0;
    unsigned int sbox = 0;
    
    
    //collect_input(plaintext);
    for(i = 0; i < 4; i++)
    {
        printf("Text: %08x\n", plaintext[i]);
    }
    


    build_tables(0xe5);
    /* Round Key Generation begins here
     * k is the iteration of key expansion (which round key out of 11) */
    for(k = 0; k < 4*R; k++)
    {
        int index = k - N;
        
        if (k < N)
        {
            memcpy(&expanded_key[0][k], &key[k], sizeof(int));
        }
        else if( (k >= N) && (N > 6) && ((k % N) == 4))
        {
            sbox = temp;
            
            temp = (get_sbox_value((sbox & 0xff000000) >> 24) << 24);
            temp = temp | get_sbox_value((sbox & 0x00ff0000) >> 16) << 16;
            temp = temp | get_sbox_value((sbox & 0x0000ff00) >> 8) << 8;
            temp = temp | get_sbox_value(sbox & 0x000000ff);
            
            temp = temp ^ expanded_key[index/N][index % N];
            
            memcpy(&expanded_key[k/N][k%N], &temp, sizeof(int));
        }
        else if( (k >= N) && ((k % N) == 0) )
        {
            temp = rotate(temp);
            
            sbox = temp;
            
            temp = (get_sbox_value((sbox & 0xff000000) >> 24) << 24);
            temp = temp | get_sbox_value((sbox & 0x00ff0000) >> 16) << 16;
            temp = temp | get_sbox_value((sbox & 0x0000ff00) >> 8) << 8;
            temp = temp | get_sbox_value(sbox & 0x000000ff);

            temp = temp ^ round_constant(k/N);
            
            
            temp = temp ^ expanded_key[index/N][index % N];
            
            memcpy(&expanded_key[k/N][k%N], &temp, sizeof(int));
        }
        else
        {
            temp = temp ^ expanded_key[index/N][index % N];
            memcpy(&expanded_key[k/N][k%N], &temp, sizeof(int));
        }
        
        temp = expanded_key[k/N][k % N];
    }

    dump_expanded_key(expanded_key);
    //exit(0);
    memcpy(&last_key[0], &plaintext[0], 4*sizeof(int));
    /* Core schedule begins here */
    for(int z = 0; z < (R - 1); z++)
    {
        printf("Round: %d\n", z+1);
        printf("%08x %08x %08x %08x\n", expanded_key[z][0], expanded_key[z][1], expanded_key[z][2], expanded_key[z][3]);
        
        
        /* Add round key */
        if( z < (R - 1))
        {
            for( i = 0; i < 4; i++)
            {
                cipher_key[i] = (last_key[i] ^ expanded_key[z][i]);
                printf("%08x ", cipher_key[i]);
            }
            printf("\n");
        }
        
        
        /* S-box substitution */
        for( w = 0; w < 4; w++)
        {
            temp = cipher_key[w];
            cipher_key[w] = 0;
            for(int byte = 0; byte < 4; byte++)
            {
                bitshift = 24 - byte*8;
                cipher_key[w] = cipher_key[w] | (get_sbox_value(temp >> (bitshift)) << (bitshift));
            }
            printf("%08x ", cipher_key[w]);
        }
    
        printf("\n");
        
        
        
        /* Shift rows */
        
        transpose_matrix(cipher_key);
        for(w = 0; w < 4; w++)
        {
            cipher_key[w] = (cipher_key[w] << 8*w) | (cipher_key[w] >> (32 - 8*w));
        }
        transpose_matrix(cipher_key);
        
        for(w = 0; w < 4; w++)
        {
            printf("%08x ", cipher_key[w]);
        }
        printf("\n");
        
        
        
        /* Mix Columns */  
        if(z < (R - 2))
        {
            unsigned char b1 = 0;
            unsigned char b2 = 0;
            unsigned char b3 = 0;
            unsigned char b4 = 0;
            
            for(w = 0; w < 4; w++)
            {
                temp = 0;
                b1 = cipher_key[w] >> 24;
                b2 = cipher_key[w] >> 16;
                b3 = cipher_key[w] >> 8;
                b4 = cipher_key[w];
                
                temp = (galois_multiplication(b1, 2)^galois_multiplication(b2, 3)^galois_multiplication(b3, 1)^galois_multiplication(b4, 1)) << 24;
                temp = temp | ((galois_multiplication(b1, 1)^galois_multiplication(b2, 2)^galois_multiplication(b3, 3)^galois_multiplication(b4, 1)) << 16);
                temp = temp | ((galois_multiplication(b1, 1)^galois_multiplication(b2, 1)^galois_multiplication(b3, 2)^galois_multiplication(b4, 3)) << 8);
                temp = temp | (galois_multiplication(b1, 3)^galois_multiplication(b2, 1)^galois_multiplication(b3, 1)^galois_multiplication(b4, 2));
                cipher_key[w] = temp;
            }
            memcpy(&last_key[0], &cipher_key[0], 4*sizeof(int));
        }
        /* Add round key */
        else
        {
            printf("%08x %08x %08x %08x\n", expanded_key[z+1][0], expanded_key[z+1][1], expanded_key[z+1][2], expanded_key[z+1][3]);
            for( i = 0; i < 4; i++)
            {
                cipher_key[i] = (cipher_key[i] ^ expanded_key[z+1][i]);
            }
            memcpy(&last_key[0], &cipher_key[0], 4*sizeof(int));
        }
        
        
        for(w = 0; w < 4; w++)
        {
            printf("%08x ", last_key[w]);
        }
        printf("\n\n");
    }
    
    //dump_expanded_key(expanded_key);


    return 0;
}

unsigned char collect_input(unsigned int text[4])
{
    unsigned char input[16];
    unsigned char status = 0;
    unsigned char i = 0;
    
    if (fgets(input, sizeof(input), stdin) != NULL)
    {
        status = 1;
        for(i = 0; i < 4; i++)
        {
            memcpy(&text[i], &input[i*4], sizeof(int));
            printf("%08X\n", text[i]);
        }
    }

    return status;
}

void transpose_matrix(unsigned int row[4])
{
    unsigned char mat[4][4] = {0};
    unsigned int temp = 0;
    
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            mat[j][i] = row[i] >> (24 - 8*j);
        }
    }
    
    
    for(int i = 0; i < 4; i++)
    {
        row[i] = 0;
        for(int j = 0; j < 4; j++)
        {
            row[i] = row[i] | (mat[i][j] << (24 - 8*j));
        }

    }
    
}

void add_round_key(unsigned int key, unsigned int round_key)
{
    return (key ^ round_key);
}

unsigned int round_constant(unsigned char num)
{
    /* initial round constant value (first round) */
    unsigned char x = 1;
    while ( num != 1)
    {
        x = galois_multiplication(x, 2);
        num--;
    }
    return x << 24;
}

unsigned int rotate(unsigned int a)
{
    return (a << 8) | (a >> 24);
}

unsigned char get_sbox_value(unsigned char a)
{
    unsigned char s = get_inverse(a);
    unsigned char x = get_inverse(a);
    unsigned char set = 0;


    for (int i=0; i<4; i++)
    {
        set = (s & 0x80);
        s = s << 1;
        if(set)
        {
            s = s | 0x01;
        }
        x = x ^ s;
    }
    x = x ^ 0x63;
    return x;
}


unsigned char get_inverse(unsigned char a)
{
    if (a == 0)
        return 0;
    else
    {
        unsigned char b = log_table[a];
        return expo_table[255 - b];
    }
}

void build_tables(unsigned char num)
{
    unsigned char e = 0;
    e = galois_multiplication(num, 1);
    expo_table[0] = 1;
    expo_table[1] = e;


    for (int i=1; i<256; i++)
    {
        e = galois_multiplication(num, e);
        log_table[e] = i+1;
        expo_table[i+1] = e;
    }
    log_table[0] = 0;
    log_table[0xe5] = 1;
}


unsigned char galois_multiplication(unsigned char a, unsigned char b)
{
    unsigned char p = 0;
    int set = 0;
    for(int i=0; i<8; i++)
    {
        set = 0;
        if(b & 0x01)
        {
            p = p ^ a;
        }
        if(a & 0x80)
        {
            set = 1;
        }
        a = a << 1;
        if (set)
        {
            a = a ^ 0x1b;
        }
        b = b >> 1;
    }
    return p;
}

void dump_expanded_key(unsigned int expanded[11][8])
{
    int rk;
    int subword;
    
    for (rk = 0; rk < R; rk++)
    {
        for (subword = 0; subword<N; subword++)
        {
            printf("%08x ", expanded[rk][subword]);
        }
        printf("\n");
    }
}
